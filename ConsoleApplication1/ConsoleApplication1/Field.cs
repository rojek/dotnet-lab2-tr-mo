﻿using System;

class Field
{

    private bool alive = false;
    private bool aliveInNextIteration = false;

    private int numberOfNeighbours = 0;



    internal void computeNextIterationValue()
    {
        if (shouldDie())
            aliveInNextIteration = false;
        else if (shouldStayAlive())
            aliveInNextIteration = true;
        else if (shouldBecomeResurected())
            aliveInNextIteration = true;
    }

    private bool shouldDie()
    {
        return numberOfNeighbours < 2 || numberOfNeighbours > 3;
    }

    private bool shouldStayAlive()
    {
        return (numberOfNeighbours == 2 || numberOfNeighbours == 3) && alive;
    }

    private bool shouldBecomeResurected()
    {
        return numberOfNeighbours == 3 && !alive;
    }





    public void markAlive()
    {
        this.alive = true;
    }

    public void markDead()
    {
        this.alive = false;
    }

    internal void setAliveToNextValue()
    {
        this.alive = this.aliveInNextIteration;
    }

    public String getAliveAsString()
    {
        return alive ? "!" : ".";
    }

    public Boolean getAlive()
    {
        return alive;
    }

    internal void setNumberOfNeighbours(int numberOfNeighbours)
    {
        this.numberOfNeighbours = numberOfNeighbours;
    }

}
