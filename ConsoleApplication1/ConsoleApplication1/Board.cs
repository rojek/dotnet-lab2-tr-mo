﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Board
    {

        public Field[,] fields { get; set; }
        public int size { get; }


        public Board(int size)
        {
            this.size = size;
            fields = new Field[size, size];
            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    fields[i, j] = new Field();
                }
            }
        }

        public void printBoard()
        {
            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    Console.Write(fields[i, j].getAliveAsString());
                }
                Console.WriteLine();
            }
        }

        public void recalculateBoard()
        {

            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    calculateNeighboursFor(i, j);
                    fields[i, j].computeNextIterationValue();
                }
            }
            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    fields[i, j].setAliveToNextValue();
                }
            }

        }

        private void calculateNeighboursFor(int x, int y)
        {
            int fieldNumberOfNeighbours = 0;

            for (int i = x - 1; i <= x + 1; i++)
            {
                for (int j = y - 1; j <= y + 1; j++)
                {
                    if (isBoundary(i, j) || isActuallyResolvedField(i, j, x, y))
                        continue;
                    if (fields[i, j].getAlive())
                    {
                        fieldNumberOfNeighbours++;
                    }
                }
            }

            fields[x, y].setNumberOfNeighbours(fieldNumberOfNeighbours);
        }

        private bool isActuallyResolvedField(int resolvedX, int resolvedY, int fieldX, int fieldY)
        {
            return resolvedX == fieldX && resolvedY == fieldY;
        }

        private bool isBoundary(int x, int y)
        {
            return (x < 0 || x >= size || y < 0 || y >= size);
        }
    }

}
