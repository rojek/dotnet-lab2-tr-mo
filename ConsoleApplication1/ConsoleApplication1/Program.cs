﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Threading.Tasks;
using ConsoleApplication1;

namespace game_of_life
{
    enum BoardInitializationStrategies { RANDOM, GLIDER};
    class Program
    {
        static void Main(string[] args)
        {
            Board board = new Board(10);
            BoardInitializer initializer = new BoardInitializer();

            initializer.initialize(BoardInitializationStrategies.RANDOM, 25, board);
            //initializer.initialize(BoardInitializationStrategies.GLIDER, 0, board);

            while (true)
            {
                board.printBoard();
                Console.WriteLine();
                Console.ReadKey();
                Console.Clear();
                board.recalculateBoard();
            }
        }
    }


}

