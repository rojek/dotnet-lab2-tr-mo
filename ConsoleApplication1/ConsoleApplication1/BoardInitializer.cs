﻿using game_of_life;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class BoardInitializer
    {
        public void initialize(BoardInitializationStrategies strategy, int numberOfAliveFields, Board board)
        {
            switch (strategy)
            {
                case BoardInitializationStrategies.GLIDER:
                    initializeGlider(board);
                    break;
                case BoardInitializationStrategies.RANDOM:
                    initializeRandom(numberOfAliveFields, board);
                    break;
                default:
                    initializeRandom(numberOfAliveFields, board);
                    break;
            }
        }

        private void initializeRandom(int numberOfAliveFields, Board board)
        {
            int toResurrect = 12;

            Random random = new Random();

            int size = board.size;

            while (toResurrect != 0)
            {
                int i = random.Next(size);
                int j = random.Next(size);

                board.fields[i, j].markAlive();
                toResurrect--;
            }
        }

        private void initializeGlider(Board board)
        {
            board.fields[4, 3].markAlive();
            board.fields[4, 4].markAlive();
            board.fields[4, 5].markAlive();
            board.fields[3, 5].markAlive();
            board.fields[2, 4].markAlive();
        }
    }
}
